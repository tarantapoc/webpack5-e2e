describe('empty spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/testdb/dashboard?id=635aa4930064c20011cf80b8');
    cy.window()
    .its('performance')
    .invoke('mark', 'beforeStart')
    cy.contains('Start').click();
    cy.contains('Start').should('not.exist');
    cy.window()
    .its('performance')
    .invoke('mark', 'START')
    cy.get('.attr_display').should('have.length', 100);
    cy.get('.attr_display').eq(99).should(($div) => {
      expect($div.text().trim()).to.not.equal("test/tarantatestdevice/1/Int_RO_052:");
    });
    cy.window()
    .its('performance')
    .invoke('measure', 'START')
  })
})