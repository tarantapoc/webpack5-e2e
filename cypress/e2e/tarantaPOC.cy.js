describe('empty spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:4001/');
    cy.window()
    .its('performance')
    .invoke('mark', 'beforeStart')
    cy.contains('RUN').click();
    cy.contains('RUN').should('not.exist');
    cy.window()
    .its('performance')
    .invoke('mark', 'START')
    cy.get('.attr_display').should('have.length', 100);
    cy.get('.attr_display').eq(99).should(($div) => {
      expect($div.text().trim()).to.not.equal("test/tarantatestdevice/1/Int_RO_100:");
    });
    cy.window()
    .its('performance')
    .invoke('measure', 'START')
  })
})